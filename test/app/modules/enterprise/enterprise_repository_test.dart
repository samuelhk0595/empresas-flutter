import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/modules/app_module/app_module.dart';
import 'package:ioasys_test/app/modules/enterprise/enterprise_repository.dart';

void main() {
  initModule(AppModule());

  final repository = EnterpriseRepository(Modular.get<RestClient>());

  group('Enterprise Types parse', () {
    final list = repository.getEnterpriseTypes();
    test('Should return a list not empty', () {
      expect(list.isNotEmpty, equals(true));
    });

    test('All objects must be not null', () {
      expect(list.contains(null), equals(false));
    });
  });
}
