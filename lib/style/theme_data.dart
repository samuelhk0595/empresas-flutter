import 'dart:ui';

import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get defaultThemeData {
    return ThemeData(
      fontFamily: 'SourceSansPro',
        primarySwatch: Colors.blue,
        primaryColor: Color(0xffE01E69),
        
        brightness: Brightness.light,
        textTheme: TextTheme(headline2: headline2, subtitle2: subtitle2));
  }

  static TextTheme get textTheme => defaultThemeData.textTheme;

  static TextStyle get headline2 {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 20, color: Color(0xff333333));
  }

  static TextStyle get subtitle2 {
    return TextStyle(color: Color(0xff333333).withOpacity(0.8));
  }
}

class _AppColors {}
