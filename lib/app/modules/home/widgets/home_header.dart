import 'package:flutter/material.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/utils/assets_paths.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Image.asset(
            AssetsPaths.logo_transparent,
          ),
          Positioned(
              right: 10,
              child: Image.asset(
                AssetsPaths.logo_3,
                height: 50,
              )),
          Positioned(
              left: 220,
              child: Image.asset(
                AssetsPaths.logo_4,
                height: 60,
              )),
          Positioned(
              right: 0,
              bottom: 30,
              child: Image.asset(
                AssetsPaths.logo_5,
                height: 100,
              )),
        ],
      ),
      width: Device.width,
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [
                0.1,
                0.9
              ],
              colors: [
                Colors.blue,
                Theme.of(context).primaryColor,
              ])),
    );
  }
}
