
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ioasys_test/app/shared/widgets/loading_screen.dart';
import 'package:ioasys_test/app/shared/widgets/textfields/custom_textfield.dart';

import '../home_controller.dart';
import 'filter_panel.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    Key key,
    @required this.controller,
    this.focusNode,
  }) : super(key: key);

  final HomeController controller;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0),
            child: Material(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              elevation: 5.0,
              child: Row(
                children: [
                  Expanded(
                    child: CustomTextField(
                      focusNode: focusNode,
                      borderColor: Colors.white,
                      focusedBorderColor: Colors.white,
                      fillColor: Colors.white,
                      hintText: 'Pesquise por uma empresa',
                      prefixIcon: Icon(Icons.search),
                      controller: controller.searchBarController,
                      onFieldSubmitted: (text) async {
                        ShowLoadingScreen(context);
                        await controller.search();
                        Modular.link.pop();
                      },
                    ),
                  ),
                  InkWell(
                    onTap: ()async {
                      await showModalBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          backgroundColor: Colors.transparent,
                          builder: (context) =>
                              FilterPanel(controller: controller));
                      FocusScope.of(context).requestFocus(focusNode);
                    },
                    child: Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            FontAwesomeIcons.filter,
                            color: Colors.white,
                            size: 9,
                          ),
                          SizedBox(width: 8),
                          Text(
                            controller.filterName,
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 8.0, 5.0, 8.0),
                      padding: EdgeInsets.fromLTRB(8, 2, 12, 2),
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 5),
          Offstage(
              offstage: controller.resultsAmount == 0,
              child: Text(
                '${controller.resultsAmount} resultados encontrados',
                style: TextStyle(color: Colors.white),
              ))
        ],
      );
    });
  }
}
