import 'package:delayed_widget/delayed_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_type_model.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/widgets/custom_button.dart';

import '../home_controller.dart';

class FilterPanel extends StatefulWidget {
  const FilterPanel({
    Key key,
    this.controller,
  }) : super(key: key);

  final HomeController controller;

  @override
  _FilterPanelState createState() => _FilterPanelState();
}

class _FilterPanelState extends State<FilterPanel> {
  EnterpriseType selectedEnterpriseType;

  @override
  void initState() {
    super.initState();
    selectedEnterpriseType = widget.controller.selectedEnterpriseType;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Selecione o filtro',
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              width: Device.width * 0.9,
              child: Wrap(
                alignment: WrapAlignment.center,
                children: _buildFiltersList(),
              ),
            ),
            DelayedWidget(
              delayDuration: Duration(milliseconds: 200),
              child: CustomButton(
                child: Text(
                  'CONFIRMAR',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  widget.controller.selectedEnterpriseType =
                      selectedEnterpriseType;
                  Modular.link.pop();
                },
                width: Device.width * 0.9,
                height: 40,
              ),
            )
          ],
        ),
      ),
      width: Device.width,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(12))),
    );
  }

  void onSelected([EnterpriseType enterpriseType]) {
    setState(() {
      selectedEnterpriseType = enterpriseType;
    });
  }

  List<Widget> _buildFiltersList() {
    final items = widget.controller.enterpriseTypes;

    List<Widget> chips = [_buildNoFilterChip()];

    chips.addAll(
        items.map<Widget>((item) => _buildCustomFilterChip(item)).toList());
    return chips;
  }

  Widget _buildCustomFilterChip(EnterpriseType enterpriseType) {
    final selected = isSelected(enterpriseType);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: FilterChip(
          selected: selected,
          selectedColor: Colors.blue,
          checkmarkColor: Colors.white,
          label: Text(
            enterpriseType.enterpriseTypeName,
            style: TextStyle(color: selected ? Colors.white : null),
          ),
          onSelected: (selected) {
            onSelected(enterpriseType);
          }),
    );
  }

  Widget _buildNoFilterChip() {
    final selected = selectedEnterpriseType == null;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: FilterChip(
          selected: selected,
          selectedColor: Colors.blue,
          checkmarkColor: Colors.white,
          label: Text(
            'Tudo',
            style: TextStyle(color: selected ? Colors.white : null),
          ),
          onSelected: (selected) {
            onSelected();
          }),
    );
  }

  bool isSelected(EnterpriseType enterpriseType) {
    if (selectedEnterpriseType == null) return false;
    return selectedEnterpriseType.id == enterpriseType.id;
  }
}
