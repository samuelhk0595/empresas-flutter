import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/modules/enterprise/enterprise_module.dart';
import 'package:ioasys_test/app/modules/enterprise/models/widgets/enterprise_card.dart';
import 'package:ioasys_test/app/modules/enterprise/pages/enterprise_details/enterprise_details_page.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/widgets/custom_loading_indicator.dart';

import 'home_controller.dart';
import 'widgets/home_header.dart';
import 'widgets/search_bar.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Observer(builder: (context) {
        return CustomScrollView(
          slivers: [
            SliverAppBar(
              leading: lgooutButton(),
              backgroundColor: Color(0xffAF1C94),
              pinned: true,
              expandedHeight: Device.height * 0.2,
              collapsedHeight: 60,
              flexibleSpace: FlexibleSpaceBar(
                background: HomeHeader(),
              ),
              bottom: PreferredSize(
                  preferredSize: Size(Device.width, 50),
                  child: SearchBar(
                    controller: controller,
                    focusNode: controller.searchBarFocusNode,
                  )),
            ),
            controller.searchResult.body == null &&
                    !controller.searchResult.isLoading &&
                    !controller.searchResult.hasError
                ? SliverToBoxAdapter(
                    child: Column(
                      children: [
                        SizedBox(height: Device.height * 0.2),
                        Icon(
                          Icons.search,
                          color: Colors.black54,
                          size: 50,
                        ),
                        Text(
                          'Faça sua pesquisa',
                          style: TextStyle(fontSize: 17),
                        ),
                      ],
                      mainAxisSize: MainAxisSize.min,
                    ),
                  )
                : controller.searchResult.isLoading
                    ? SliverToBoxAdapter(
                        child: CustomLoadingIndicator(),
                      )
                    : controller.searchResult.hasError
                        ? SliverToBoxAdapter(
                            child: Column(
                              children: [
                                SizedBox(height: Device.height * 0.2),
                                Icon(
                                  Icons.error,
                                  color: Colors.black54,
                                  size: 50,
                                ),
                                Text(
                                  'Ocorreu um erro ao fazer a pesquisa',
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                              mainAxisSize: MainAxisSize.min,
                            ),
                          )
                        : controller.searchResult.body.isEmpty
                            ? SliverToBoxAdapter(
                                child: Column(
                                  children: [
                                    SizedBox(height: Device.height * 0.2),
                                    Icon(
                                      Icons.search,
                                      color: Colors.black54,
                                      size: 50,
                                    ),
                                    Text(
                                      'Nenhum resultado encontrado',
                                      style: TextStyle(fontSize: 17),
                                    ),
                                  ],
                                  mainAxisSize: MainAxisSize.min,
                                ),
                              )
                            : SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (context, index) {
                                final enterprise =
                                    controller.searchResult.body[index];
                                return EnterpriseCard(
                                  enterprise: enterprise,
                                  onTap: () {
                                    Modular.to.pushNamed(
                                        EnterpriseModule.into(
                                            EnterpriseDetailsPage.routeName),
                                        arguments: enterprise);
                                  },
                                );
                              },
                                    childCount:
                                        controller.searchResult.body.length)),
          ],
        );
      }),
    );
  }

  InkWell lgooutButton() {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 8, 0, 8),
        child: Row(
          children: [
            Icon(
              Icons.logout,
              size: 14,
            ),
            Text('Sair')
          ],
        ),
      ),
      onTap: controller.logout,
    );
  }
}
