// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $HomeController = BindInject(
  (i) => HomeController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  Computed<String> _$filterNameComputed;

  @override
  String get filterName =>
      (_$filterNameComputed ??= Computed<String>(() => super.filterName,
              name: '_HomeControllerBase.filterName'))
          .value;

  final _$searchResultAtom = Atom(name: '_HomeControllerBase.searchResult');

  @override
  AppResponse<List<Enterprise>> get searchResult {
    _$searchResultAtom.reportRead();
    return super.searchResult;
  }

  @override
  set searchResult(AppResponse<List<Enterprise>> value) {
    _$searchResultAtom.reportWrite(value, super.searchResult, () {
      super.searchResult = value;
    });
  }

  final _$selectedEnterpriseTypeAtom =
      Atom(name: '_HomeControllerBase.selectedEnterpriseType');

  @override
  EnterpriseType get selectedEnterpriseType {
    _$selectedEnterpriseTypeAtom.reportRead();
    return super.selectedEnterpriseType;
  }

  @override
  set selectedEnterpriseType(EnterpriseType value) {
    _$selectedEnterpriseTypeAtom
        .reportWrite(value, super.selectedEnterpriseType, () {
      super.selectedEnterpriseType = value;
    });
  }

  final _$searchAsyncAction = AsyncAction('_HomeControllerBase.search');

  @override
  Future<void> search() {
    return _$searchAsyncAction.run(() => super.search());
  }

  @override
  String toString() {
    return '''
searchResult: ${searchResult},
selectedEnterpriseType: ${selectedEnterpriseType},
filterName: ${filterName}
    ''';
  }
}
