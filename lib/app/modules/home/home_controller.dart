import 'package:flutter/cupertino.dart';
import 'package:ioasys_test/app/core/app_response.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/core/session/session.dart';
import 'package:ioasys_test/app/modules/enterprise/enterprise_repository.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_model.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_type_model.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'home_controller.g.dart';

@Injectable()
class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final _repository = EnterpriseRepository(Modular.get<RestClient>());
  final searchBarController = TextEditingController();
  final searchBarFocusNode = FocusNode();

  @observable
  AppResponse<List<Enterprise>> searchResult = AppResponse();

  @action
  Future<void> search() async {
    try {
      searchResult = AppResponse.loading();
      final enterprises = await _repository.search(
          searchBarController.text.trim(),
          enterpriseType: selectedEnterpriseType);
      searchResult = AppResponse.completed(enterprises);
    } catch (error) {
      searchResult = AppResponse.error(error);
    }
  }

  int get resultsAmount => searchResult.body?.length ?? 0;

  void logout() {
    Modular.get<Session>().logout();
  }

  @observable
  EnterpriseType selectedEnterpriseType;

  @computed
  String get filterName {
    if (selectedEnterpriseType == null) return 'Tudo';
    return selectedEnterpriseType.enterpriseTypeName;
  }

  List<EnterpriseType> get enterpriseTypes {
    return _repository.getEnterpriseTypes();
  }
}
