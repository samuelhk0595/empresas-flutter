// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $AuthenticationController = BindInject(
  (i) => AuthenticationController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AuthenticationController on _AuthenticationControllerBase, Store {
  final _$authenticationAtom =
      Atom(name: '_AuthenticationControllerBase.authentication');

  @override
  AppResponse<dynamic> get authentication {
    _$authenticationAtom.reportRead();
    return super.authentication;
  }

  @override
  set authentication(AppResponse<dynamic> value) {
    _$authenticationAtom.reportWrite(value, super.authentication, () {
      super.authentication = value;
    });
  }

  final _$emailErrorMessageAtom =
      Atom(name: '_AuthenticationControllerBase.emailErrorMessage');

  @override
  String get emailErrorMessage {
    _$emailErrorMessageAtom.reportRead();
    return super.emailErrorMessage;
  }

  @override
  set emailErrorMessage(String value) {
    _$emailErrorMessageAtom.reportWrite(value, super.emailErrorMessage, () {
      super.emailErrorMessage = value;
    });
  }

  final _$passwordErrorMessageAtom =
      Atom(name: '_AuthenticationControllerBase.passwordErrorMessage');

  @override
  String get passwordErrorMessage {
    _$passwordErrorMessageAtom.reportRead();
    return super.passwordErrorMessage;
  }

  @override
  set passwordErrorMessage(String value) {
    _$passwordErrorMessageAtom.reportWrite(value, super.passwordErrorMessage,
        () {
      super.passwordErrorMessage = value;
    });
  }

  final _$signInAsyncAction =
      AsyncAction('_AuthenticationControllerBase.signIn');

  @override
  Future<void> signIn() {
    return _$signInAsyncAction.run(() => super.signIn());
  }

  @override
  String toString() {
    return '''
authentication: ${authentication},
emailErrorMessage: ${emailErrorMessage},
passwordErrorMessage: ${passwordErrorMessage}
    ''';
  }
}
