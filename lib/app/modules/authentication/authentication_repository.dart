import 'package:dio/dio.dart';
import 'package:ioasys_test/app/core/env.dart';
import 'package:ioasys_test/app/core/rest_client.dart';

class AuthenticationRepository {
  AuthenticationRepository(this.restClient);

  final RestClient restClient;

  final String signInEndpoint = '/api/${Env.apiVersion}/users/auth/sign_in';

  Future<Map> signIn({String email, String password}) async {
    final Response response = await restClient.post(
        url: signInEndpoint, data: {"email": email, "password": password});

    if (response == null || !response.data['success']) throw Error();

    Map headers = {};

    headers['Content-Type'] = 'application/json';
    headers['access-token'] = response.headers.map['access-token'];
    headers['client'] = response.headers.map['client'];
    headers['uid'] = response.headers.map['uid'];

    return {'investor': response.data['investor'], 'headers': headers};
  }
}
