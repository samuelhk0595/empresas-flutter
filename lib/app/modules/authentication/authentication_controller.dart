import 'package:flutter/cupertino.dart';
import 'package:ioasys_test/app/core/app_response.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/core/session/session.dart';
import 'package:ioasys_test/app/modules/app_module/pages/app_init/app_init_page.dart';
import 'package:ioasys_test/app/modules/authentication/authentication_repository.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'authentication_controller.g.dart';

@Injectable()
class AuthenticationController = _AuthenticationControllerBase
    with _$AuthenticationController;

abstract class _AuthenticationControllerBase with Store {
  final _repository = AuthenticationRepository(Modular.get<RestClient>());
  final emailController =
      TextEditingController(text: 'testeapple@ioasys.com.br');

  final passwordController = TextEditingController(text: '12341234');
  

  @observable
  AppResponse authentication = AppResponse();

  @action
  Future<void> signIn() async {
      try {
        authentication = AppResponse.loading();
        final response = await _repository.signIn(
            email: emailController.text.trim(),
            password: passwordController.text);
        Modular.get<Session>().setUp(response);
        authentication = AppResponse.completed(response);
        Modular.to.pushReplacementNamed(AppInitPage.routeName);
      } catch (error, stacktrace) {
        String message = 'Ocorreu um erro ao tentar entrar';
        if (error.response.data['errors'].first ==
            'Invalid login credentials. Please try again.') {
          message = 'Credenciais incorretas';
        }
        emailErrorMessage = '';
        passwordErrorMessage = message;
        authentication = AppResponse.error(error, message: message);
      }
  }

  @observable
  String emailErrorMessage;

  @observable
  String passwordErrorMessage;
}
