import 'authentication_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'authentication_page.dart';

class AuthenticationModule extends ChildModule {
  static const String routeName = '/AuthenticationModule';
  @override
  List<Bind> get binds => [
        $AuthenticationController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => AuthenticationPage()),
      ];

  static Inject get to => Inject<AuthenticationModule>.of();
}
