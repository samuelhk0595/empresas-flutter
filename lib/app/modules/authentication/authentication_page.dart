import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/modules/authentication/widgets/authentication_header.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/widgets/custom_button.dart';
import 'package:ioasys_test/app/shared/widgets/loading_screen.dart';
import 'package:ioasys_test/app/shared/widgets/textfields/email_textfield.dart';
import 'package:ioasys_test/app/shared/widgets/textfields/password_textfield.dart';

import 'authentication_controller.dart';

class AuthenticationPage extends StatefulWidget {
  final String title;
  const AuthenticationPage({Key key, this.title = "Authentication"})
      : super(key: key);

  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState
    extends ModularState<AuthenticationPage, AuthenticationController> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Observer(builder: (context) {
          return Center(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AuthenticationPageHeader(),
                  Column(
                    children: [
                      SizedBox(height: Device.height * 0.1),
                      EmailTextField(
                        labelText: 'Email',
                        hintText: '',
                        width: Device.width * 0.9,
                        errorText: controller.emailErrorMessage,
                        enabled: !controller.authentication.isLoading,
                        controller: controller.emailController,
                      ),
                      SizedBox(height: 15),
                      PasswordTextField(
                        labelText: 'Senha',
                        width: Device.width * 0.9,
                        errorText: controller.passwordErrorMessage,
                        enabled: !controller.authentication.isLoading,
                        hintText: '',
                        controller: controller.passwordController,
                      ),
                      SizedBox(height: 35),
                      CustomButton(
                        width: Device.width * 0.9,
                        height: 50,
                        showLoading: controller.authentication.isLoading,
                        child: Text(
                          'ENTRAR',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () async {
                          if (formKey.currentState.validate()) {
                            ShowLoadingScreen(context);
                            await controller.signIn();
                            if (!controller.authentication.isCompleted)
                              Modular.link.pop();
                          }
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
