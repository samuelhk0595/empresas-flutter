import 'package:delayed_widget/delayed_widget.dart';
import 'package:flutter/material.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/utils/assets_paths.dart';

class AuthenticationPageHeader extends StatefulWidget {
  const AuthenticationPageHeader({
    Key key,
  }) : super(key: key);

  @override
  _AuthenticationPageHeaderState createState() =>
      _AuthenticationPageHeaderState();
}

class _AuthenticationPageHeaderState extends State<AuthenticationPageHeader> {
  double width;
  double height;
  double messageOpacity;

  @override
  void initState() {
    super.initState();
    width = Device.width;
    height = Device.height * 0.3;
    messageOpacity = 1.0;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await Future.delayed(Duration(seconds: 2));
      setState(() {
        width = Device.width;
        height = Device.height * 0.2;
        messageOpacity = 0.0;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: Cropper(),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AssetsPaths.ioasys_logo_home,
                width: Device.width * 0.3),
            DelayedWidget(
              delayDuration: Duration(milliseconds: 100),
                          child: AnimatedOpacity(
                duration: Duration(milliseconds: 400),
                curve: Curves.easeOut,
                opacity: messageOpacity,
                child: Text(
                  'Seja bem vindo ao Empresas!',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 24),
                ),
              ),
            )
          ],
        ),
        alignment: Alignment.center,
        width: width,
        height: height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                stops: [
              0.1,
              0.9
            ],
                colors: [
              Colors.blue,
              Theme.of(context).primaryColor,
            ])),
      ),
    );
  }
}

class Cropper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - 40);
    path.quadraticBezierTo(size.width * 0.5, size.height, 0, size.height - 40);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
