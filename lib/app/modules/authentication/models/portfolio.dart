class Portfolio {
  int enterprisesNumber;
  List enterprises;

  Portfolio({this.enterprisesNumber, this.enterprises});

  Portfolio.fromJson(Map<dynamic, dynamic> json) {
    enterprisesNumber = json['enterprises_number'];
    enterprises = json['enterprises'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enterprises_number'] = this.enterprisesNumber;
    if (this.enterprises != null) {
      data['enterprises'] = this.enterprises;
    }
    return data;
  }
}