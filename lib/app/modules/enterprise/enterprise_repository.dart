import 'package:dio/dio.dart';
import 'package:ioasys_test/app/core/env.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_type_model.dart';

import 'models/enterprise_model.dart';

class EnterpriseRepository {
  EnterpriseRepository(this.restClient);

  final RestClient restClient;

  final String _enterpriseSearchEndpoint =
      '/api/${Env.apiVersion}/enterprises?name=';


  Future<List<Enterprise>> search(String enterpriseName,
      {EnterpriseType enterpriseType}) async {
    Response response;

    if (enterpriseType == null) {
      response = await _searchWithoutFilters(enterpriseName);
    } else {
      response = await _searchWithFilters(enterpriseName, enterpriseType);
    }

    if (response == null) throw Error();

    return response.data['enterprises']
        .map<Enterprise>((json) => Enterprise.fromJson(json))
        .toList();
  }

  Future<Response> _searchWithoutFilters(String enterpriseName) {
    return restClient.get(url: _enterpriseSearchEndpoint + enterpriseName);
  }

  Future<Response> _searchWithFilters(
      String enterpriseName, EnterpriseType enterpriseType) {
    return restClient.get(
        url: _buildSearchWithFiltersUrl(enterpriseName, enterpriseType.id));
  }

  String _buildSearchWithFiltersUrl(
      String enterpriseName, int enterpriseTypeId) {
    return '/api/${Env.apiVersion}/enterprises?enterprise_types=$enterpriseTypeId&name=$enterpriseName';
  }

  List<EnterpriseType> getEnterpriseTypes() {
    final items = _getEnterpriseTypesJsonList();
    return items
        .map<EnterpriseType>((e) => EnterpriseType.fromJson(e))
        .toList();
  }

  List<Map<String, dynamic>> _getEnterpriseTypesJsonList() {
    return <Map<String, dynamic>>[
      {'id': 5, 'enterprise_type_name': "Biotechnology"},
      {'id': 10, 'enterprise_type_name': "Education"},
      {'id': 2, 'enterprise_type_name': "Fintech"},
      {'id': 16, 'enterprise_type_name': "Food"},
      {'id': 15, 'enterprise_type_name': "Games"},
      {'id': 3, 'enterprise_type_name': "Health"},
      {'id': 7, 'enterprise_type_name': "IT"},
      {'id': 26, 'enterprise_type_name': "IOT"},
      {'id': 12, 'enterprise_type_name': "Service"},
      {'id': 13, 'enterprise_type_name': "Social"},
      {'id': 11, 'enterprise_type_name': "Software"},
      {'id': 24, 'enterprise_type_name': "Transport"},
      {'id': 21, 'enterprise_type_name': "Marketplace"},
      {'id': 6, 'enterprise_type_name': "HR Tech"},
      {'id': 27, 'enterprise_type_name': "Music"},
      {'id': 19, 'enterprise_type_name': "Sports"},
      {'id': 28, 'enterprise_type_name': "Green"},
      {'id': 4, 'enterprise_type_name': "Construction"},
      {'id': 23, 'enterprise_type_name': "Industry"},
    ];
  }
}
