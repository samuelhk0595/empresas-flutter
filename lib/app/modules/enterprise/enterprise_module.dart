import 'package:ioasys_test/app/modules/enterprise/pages/enterprise_details/enterprise_details_page.dart';

import 'pages/enterprise_details/enterprise_details_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

class EnterpriseModule extends ChildModule {
  static const String routeName = '/EnterpriseModule';
  static String into(String route) => routeName + route;

  @override
  List<Bind> get binds => [
        $EnterpriseDetailsController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(EnterpriseDetailsPage.routeName,
            child: (_, args) => EnterpriseDetailsPage(enterprise: args.data),
            transition: TransitionType.fadeIn,
            duration: Duration(milliseconds: 300)),
      ];

  static Inject get to => Inject<EnterpriseModule>.of();
}
