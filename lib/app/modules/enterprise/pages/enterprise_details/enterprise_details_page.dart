import 'package:delayed_widget/delayed_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_model.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'enterprise_details_controller.dart';

class EnterpriseDetailsPage extends StatefulWidget {
  static const String routeName = '/EnterpriseDetailsPage';
  final Enterprise enterprise;
  const EnterpriseDetailsPage({Key key, this.enterprise}) : super(key: key);

  @override
  _EnterpriseDetailsPageState createState() => _EnterpriseDetailsPageState();
}

class _EnterpriseDetailsPageState
    extends ModularState<EnterpriseDetailsPage, EnterpriseDetailsController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: 300,
            stretch: true,
            stretchTriggerOffset: 50.0,
            onStretchTrigger: () {
              return;
            },
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: [
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
                StretchMode.zoomBackground,
              ],
              title: Text(widget.enterprise.enterpriseName),
              centerTitle: true,
              background: Hero(
                tag: widget.enterprise.id,
                child: Image.network(
                  widget.enterprise.getPictureUrl(),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
              child: DelayedWidget(
                delayDuration: Duration(milliseconds:200 ),
                              child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sessionTitle('Localização'),
                  Text(
                    '${widget.enterprise.city}, ${widget.enterprise.country}',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 20),
                  sessionTitle('Mercado'),
                  Text(
                    widget.enterprise.enterpriseType.enterpriseTypeName,
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 20),
                  sessionTitle('Cotação das ações'),
                  Text(
                    '\$ ${widget.enterprise.sharePrice}',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 20),
                  sessionTitle('Descrição'),
                  Text(
                    widget.enterprise.description,
                    style: TextStyle(
                      fontSize: 17,
                    ),
                  ),
                ],
            ),
          ),
              )),
          SliverToBoxAdapter(
            child: SizedBox(height: 30),
          )
        ],
      ),
    );
  }

  Widget sessionTitle(String title) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
            SizedBox(height: 5),
      ],
    );
  }
}
