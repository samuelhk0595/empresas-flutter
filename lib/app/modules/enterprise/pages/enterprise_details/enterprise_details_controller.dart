import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'enterprise_details_controller.g.dart';

@Injectable()
class EnterpriseDetailsController = _EnterpriseDetailsControllerBase
    with _$EnterpriseDetailsController;

abstract class _EnterpriseDetailsControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
