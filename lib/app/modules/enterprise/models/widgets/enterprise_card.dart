import 'package:flutter/material.dart';
import 'package:ioasys_test/app/modules/enterprise/models/enterprise_model.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:kenburns/kenburns.dart';

class EnterpriseCard extends StatelessWidget {
  const EnterpriseCard({Key key, this.onTap, this.enterprise})
      : super(key: key);

  final Enterprise enterprise;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(vertical: 10, horizontal: Device.width * 0.05),
      child: InkWell(
        onTap: onTap,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Hero(
            tag: enterprise.id,
            child: Container(
              color: Colors.blue,
              height: Device.width * 0.5,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    height: Device.width * 0.5,
                    child: KenBurns(
                      maxScale: 1.2,
                      maxAnimationDuration: Duration(seconds: 10),
                      minAnimationDuration: Duration(seconds: 5),
                      child: Image.network(
                        enterprise.getPictureUrl(),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.black.withOpacity(0.4),
                    height: Device.width * 0.5,
                  ),
                  Material(
                    color: Colors.transparent,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          enterprise.enterpriseName,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 25,
                          ),
                        ),
                        Text(
                          enterprise.enterpriseType.enterpriseTypeName,
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              alignment: Alignment.center,
            ),
          ),
        ),
      ),
    );
  }
}
