import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';
import 'package:ioasys_test/app/shared/utils/assets_paths.dart';

import 'app_init_controller.dart';

class AppInitPage extends StatefulWidget {
  static const String routeName = '/AppInitPage';
  const AppInitPage({Key key}) : super(key: key);

  @override
  _AppInitPageState createState() => _AppInitPageState();
}

class _AppInitPageState extends ModularState<AppInitPage, AppInitController> {
  @override
  void initState() {
    super.initState();

    controller.initApplication();
  }

  @override
  Widget build(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light));
    Device.init(context);

    return Scaffold(
      body: Container(
        width: Device.width,
        height: Device.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(height: 50),
            Image.asset(AssetsPaths.ioasys_logo_splash_screen,
                width: Device.width * 0.3),
            Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                  strokeWidth: 1.4,
                ),
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
           gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [
                0.1,
                0.9
              ],
              colors: [
                Colors.blue,
                Theme.of(context).primaryColor,
              ])),
      ),
    );
  }
}
