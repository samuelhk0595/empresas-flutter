import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/core/local_storage.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/core/session/session.dart';
import 'package:ioasys_test/app/modules/authentication/authentication_module.dart';
import 'package:ioasys_test/app/modules/home/home_module.dart';
import 'package:mobx/mobx.dart';

part 'app_init_controller.g.dart';

@Injectable()
class AppInitController = _AppInitControllerBase with _$AppInitController;

abstract class _AppInitControllerBase with Store {
  final session = Modular.get<Session>();

  Future<void> initApplication() async {
    await Future.delayed(Duration(seconds: 2));
    await Modular.get<LocalStorageManager>().init();
    await Modular.get<Session>().recover();
    Modular.get<RestClient>().setCustomHeaders(Modular.get<Session>().headers);
    if (session.isLoggedIn) {
      Modular.to.pushReplacementNamed(HomeModule.routeName);
    } else {
      Modular.to.pushReplacementNamed(AuthenticationModule.routeName);
    }
  }
}
