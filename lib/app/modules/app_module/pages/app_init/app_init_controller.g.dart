// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_init_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $AppInitController = BindInject(
  (i) => AppInitController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppInitController on _AppInitControllerBase, Store {
  @override
  String toString() {
    return '''

    ''';
  }
}
