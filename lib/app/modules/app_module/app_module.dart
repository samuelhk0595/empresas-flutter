import 'package:ioasys_test/app/core/env.dart';
import 'package:ioasys_test/app/core/local_storage.dart';
import 'package:ioasys_test/app/core/rest_client.dart';
import 'package:ioasys_test/app/core/session/session.dart';
import 'package:ioasys_test/app/modules/app_module/pages/app_init/app_init_page.dart';
import 'package:ioasys_test/app/modules/authentication/authentication_module.dart';
import 'package:ioasys_test/app/modules/enterprise/enterprise_module.dart';
import 'package:ioasys_test/app/modules/home/home_module.dart';

import 'app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';

import 'app_widget.dart';
import 'pages/app_init/app_init_controller.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        $AppController,
        $AppInitController,
        Bind((i) => RestClient(Env.baseUrl), singleton: true),
        Bind((i) => LocalStorageManager(), singleton: true),
        Bind((i) => Session(i.get<LocalStorageManager>()), singleton: true),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => AppInitPage()),
        ModularRouter(AppInitPage.routeName, child: (_, args) => AppInitPage()),
        ModularRouter(AuthenticationModule.routeName,
            module: AuthenticationModule()),
        ModularRouter(HomeModule.routeName, module: HomeModule()),
        ModularRouter(EnterpriseModule.routeName, module: EnterpriseModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
