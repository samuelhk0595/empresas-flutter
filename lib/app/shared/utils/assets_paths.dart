class AssetsPaths {
  static const String ioasys_logo_splash_screen =
      'assets/images/logo_splash_screen.png';
  static const String ioasys_logo_home = 'assets/images/logo_home.png';
  static const String logo_transparent = 'assets/images/logo_transparent.png';
  static const String inner_ellipse = 'assets/images/inner_elipse.png';
  static const String outside_ellipse = 'assets/images/outside_ellipse.png';
  static const String logo_3 = 'assets/images/logo_3.png';
  static const String logo_4 = 'assets/images/logo_4.png';
  static const String logo_5 = 'assets/images/logo_5.png';
}
