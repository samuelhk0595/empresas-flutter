import 'package:flutter/material.dart';
import 'package:ioasys_test/app/shared/tools/device_info.dart';

import 'custom_loading_indicator.dart';

class ShowLoadingScreen {
  ShowLoadingScreen(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            alignment: Alignment.center,
            child: CustomLoadingIndicator(),
            color: Colors.black.withOpacity(0.5),
            width: Device.width,
            height: Device.height,
          );
        });
  }
}
