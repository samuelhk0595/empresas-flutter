import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    @required this.child,
    this.color,
    this.width,
    this.height,
    this.borderSide,
    this.showLoading = false,
    @required this.onPressed,
  }) : super(key: key);

  final double width;
  final double height;
  final Widget child;
  final Function onPressed;
  final Color color;
  final bool showLoading;
  final BorderSide borderSide;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: IgnorePointer(
        ignoring: showLoading,
        child: FlatButton(
          child: showLoading
              ? SizedBox(
                  width: 15,
                  height: 15,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                    strokeWidth: 1.4,
                  ))
              : child,
          onPressed: onPressed,
          color: color ?? Theme.of(context).primaryColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: borderSide??BorderSide(color: Colors.transparent)),
        ),
      ),
    );
  }
}
