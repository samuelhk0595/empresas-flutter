import 'package:flutter/material.dart';
import 'package:ioasys_test/app/shared/utils/assets_paths.dart';

class CustomLoadingIndicator extends StatefulWidget {
  CustomLoadingIndicator({Key key, this.width = 60, this.height = 60})
      : super(key: key);

  final double width;
  final double height;

  @override
  _CustomLoadingIndicatorState createState() => _CustomLoadingIndicatorState();
}

class _CustomLoadingIndicatorState extends State<CustomLoadingIndicator>
    with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;


  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: Duration(seconds: 1), vsync: this)
          ..repeat();
    animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(animationController);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animation,
        builder: (context, snapshot) {
          return Container(
            width: widget.width,
            height: widget.height,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Transform.rotate(
                  angle: animation.value * 6.0,
                  child: Image.asset(
                    AssetsPaths.outside_ellipse,
                    width: widget.width - 20,
                    height: widget.height - 20,
                  ),
                ),
                Transform.rotate(
                  angle: 6.0 - (animation.value * 6.0),
                  child: Image.asset(
                    AssetsPaths.inner_ellipse,
                    width: widget.width - 40,
                    height: widget.height - 40,
                  ),
                ),
              ],
            ),
          );
        });
  }

  
  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
