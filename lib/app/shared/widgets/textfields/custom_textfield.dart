import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField(
      {Key key,
      this.hintText,
      this.width = double.infinity,
      this.isPassword = false,
      this.controller,
      this.validator,
      this.keyboardType,
      this.textInputAction,
      this.maxLength,
      this.maxLines = 1,
      this.suffixIcon,
      this.prefixIcon,
      this.counterText,
      this.isRequired = false,
      this.enabled = true,
      this.onFieldSubmitted,
      this.focusNode,
      this.inputFormatters,
      this.onChanged,
      this.suffix,
      this.contentPadding,
      this.errorText,
      this.fillColor,
      this.focusedBorderColor,
      this.borderColor,
      this.labelText})
      : super(key: key);
  final String hintText;
  final double width;
  final bool isPassword;
  final String Function(String) validator;
  final TextEditingController controller;
  final String labelText;
  final TextInputAction textInputAction;
  final int maxLength;
  final int maxLines;
  final String counterText;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Widget suffix;
  final List<TextInputFormatter> inputFormatters;
  final TextInputType keyboardType;
  final bool enabled;
  final FocusNode focusNode;
  final EdgeInsets contentPadding;
  final void Function(String) onFieldSubmitted;
  final void Function(String) onChanged;
  final bool isRequired;
  final String errorText;
  final Color fillColor;
  final Color focusedBorderColor;
  final Color borderColor;

  @override
  _CustomtextFieldState createState() => _CustomtextFieldState();
}

class _CustomtextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Offstage(
          offstage: widget.labelText == null,
          child: Text(
            widget.labelText ?? '',
            style: TextStyle(
                color: Color(0xff8a8b8b),
                fontSize: 16,
                fontWeight: FontWeight.w600),
          ),
        ),
        Offstage(
            offstage: widget.labelText == null, child: SizedBox(height: 5)),
        SizedBox(
          width: widget.width,
          child: TextFormField(
            maxLines: widget.maxLines,
            onChanged: widget.onChanged,
            focusNode: widget.focusNode,
            onFieldSubmitted: widget.onFieldSubmitted,
            enabled: widget.enabled,
            keyboardType: widget.keyboardType,
            inputFormatters: widget.inputFormatters,
            textInputAction: widget.textInputAction,
            controller: widget.controller,
            obscureText: widget.isPassword,
            validator: widget.validator ??
                (text) {
                  if (widget.isRequired) {
                    if (text.isEmpty) return 'Preencha este campo';
                    return null;
                  }
                  return null;
                },
            maxLength: widget.maxLength,
            style: TextStyle(
                fontWeight: FontWeight.normal, color: Color(0xFF7f7f7f)),
            decoration: InputDecoration(
              fillColor: widget.fillColor ?? Color(0xfff5f5f5),
              filled: true,
              errorText: widget.errorText,
              suffixIcon: widget.errorText == null
                  ? widget.suffixIcon
                  : Icon(
                      Icons.close,
                      color: Colors.red,
                    ),
              prefixIcon: widget.prefixIcon,
              suffix: widget.suffix,
              counterText: widget.counterText,
              contentPadding:
                  widget.contentPadding ?? const EdgeInsets.only(left: 25),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                  fontWeight: FontWeight.normal, color: Color(0xFF7f7f7f)),
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFCECECE), width: 0.5)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: widget.focusedBorderColor ??
                          Theme.of(context).primaryColor)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xffE4E9F2))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: widget.borderColor??Color(0xfff5f5f5))),
            ),
          ),
        ),
      ],
    );
  }
}
