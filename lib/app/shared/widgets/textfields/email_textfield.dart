import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'custom_textfield.dart';

class EmailTextField extends StatelessWidget {
  const EmailTextField({
    Key key,
    this.width,
    this.controller,
    this.focusNode,
    this.customValidator,
    this.hintText,
    this.labelText,
    this.enabled,
    this.errorText,
    this.textInputAction,
  }) : super(key: key);
  final TextEditingController controller;
  final String Function(String) customValidator;
  final String labelText;
  final String hintText;
  final bool enabled;
  final FocusNode focusNode;
  final double width;
  final String errorText;
  final TextInputAction textInputAction;

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      width: width,
      focusNode: focusNode,
      enabled: enabled ?? true,
      labelText: labelText,
      errorText: errorText,     
      hintText: hintText ?? 'Informe seu email',
      textInputAction: textInputAction,
      controller: controller,
      keyboardType: TextInputType.emailAddress,
      validator: customValidator ?? defaultValidator,
    );
  }

  String defaultValidator(String text) {
    if (text.isEmpty) {
      return 'Preencha com seu email';
    } else if (!controller.text.contains('@')) {
      return 'Esse endereço de email não é válido';
    }
    return null;
  }
}
