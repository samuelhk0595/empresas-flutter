import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'custom_textfield.dart';

class PasswordTextField extends StatefulWidget {
  PasswordTextField(
      {Key key,
      this.hintText,
      this.labelText,
      this.controller,
      this.errorText,
      this.textInputAction,
      this.enabled,
      this.width,
      this.focusNode,
      this.customValidator})
      : super(key: key);
  final String labelText;
  final String hintText;
  final TextEditingController controller;
  final String Function(String) customValidator;
  final TextInputAction textInputAction;
  final bool enabled;
  final FocusNode focusNode;
  final double width;
  final String errorText;
  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool showPassword;

  @override
  void initState() {
    super.initState();
    showPassword = false;
  }

  void switchPasswordShowing() => setState(() => showPassword = !showPassword);

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      width: widget.width,
      errorText: widget.errorText,
      enabled: widget.enabled ?? true,
      suffixIcon: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: showPassword
              ? Icon(
                  FontAwesomeIcons.eyeSlash,
                  size: 20,
                  color: Color(0xffE4E9F2),
                )
              : Icon(
                  FontAwesomeIcons.eye,
                  size: 20,
                  color: Color(0xFFD4E1F8),
                ),
          onTap: switchPasswordShowing,
        ),
      ),
      labelText: widget.labelText,
      hintText: widget.hintText ?? 'Informe sua senha',
      isPassword: !showPassword,
      textInputAction: widget.textInputAction ?? TextInputAction.next,
      controller: widget.controller,
      validator: widget.customValidator ?? defaultValidator,
    );
  }

  String defaultValidator(String text) {
    if (text.isEmpty) {
      return 'Insira sua senha';
    }
    return null;
  }
}
