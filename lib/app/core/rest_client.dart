import 'package:dio/dio.dart';

class RestClient {
  RestClient(String baseUrl) {
    _dioClient.options.baseUrl = baseUrl;
  }

  Map<String, dynamic> _customHeaders;

  void setCustomHeaders(Map headers) {
    _customHeaders = Map.castFrom<dynamic,dynamic,String,dynamic>(headers);
  }

  final _dioClient = Dio();

  Future<Response> post({String url, dynamic data}) {
    return _dioClient.post(url, data: data);
  }

  Future<Response> get({String url, dynamic data}) {
    return _dioClient.get(url, options: Options(headers: _customHeaders));
  }
}
