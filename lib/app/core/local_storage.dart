import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class LocalStorageManager {
  LocalStorageManager() {
    init();
  }

  Future<void> init() async {
    final dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
  }

  Future<Box> getReference(String document) async {
    try {
      final box = await Hive.openBox(document);
      return box;
    } catch (error) {
      await init();
      return Hive.openBox(document);
    }
  }
}