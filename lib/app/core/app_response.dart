class AppResponse<T> {
  T body;
  String message;
  dynamic error;
  AppStatus _status;
  
  AppResponse({this.body,this.error,this.message});
  
  AppResponse.loading({String message}) {
    _status = AppStatus.LOADING;
  }
  AppResponse.completed(this.body) {
    _status = AppStatus.COMPLETED;
  }
  AppResponse.error(this.error, {String message}) {
    _status = AppStatus.ERROR;
    this.message = message;
  }

  AppStatus get status => _status;

  bool get isLoading => _status == AppStatus.LOADING;

  bool get hasError => _status == AppStatus.ERROR;

  bool get isCompleted => _status == AppStatus.COMPLETED;
}

enum AppStatus { LOADING, ERROR, COMPLETED }
