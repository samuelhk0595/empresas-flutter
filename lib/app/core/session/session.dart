import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasys_test/app/core/local_storage.dart';
import 'package:ioasys_test/app/core/session/session_repository.dart';
import 'package:ioasys_test/app/modules/authentication/authentication_module.dart';
import 'package:ioasys_test/app/modules/authentication/models/investor.dart';

class Session {
  Session(LocalStorageManager _localStorageManager) {
    _repository = SessionRepository(_localStorageManager);
  }

  SessionRepository _repository;

  void setUp(Map data) {
    _fromJson(data);
    try {
      _repository.save(_toJson());
    } catch (error) {
      print(error);
    }
  }

  Future<void> recover() async {
    try {
      final response = await _repository.recover();
      _fromJson(response);
    } catch (error) {
      print(error);
    }
  }

  void logout() {
    _repository.logout();
    Modular.to.pushReplacementNamed(AuthenticationModule.routeName);
  }

  Investor _investor;

  Investor get investor => _investor;

  Map _headers;

  Map get headers => _headers;

  bool get isLoggedIn => _investor != null;

  Map _toJson() {
    return {'investor': _investor.toJson(), 'headers': _headers};
  }

  void _fromJson(Map json) {
    _investor = Investor.fromJson(json['investor']);
    _headers = json['headers'];
  }
}
