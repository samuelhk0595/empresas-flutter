import 'package:ioasys_test/app/core/local_storage.dart';

class SessionRepository {
  SessionRepository(this._localStorageManager);
  final String _sessionKey = 'session';
  final LocalStorageManager _localStorageManager;

  Future<Map> recover() async {
    final reference = await _localStorageManager.getReference(_sessionKey);
    if (!reference.containsKey('data')) return null;
    return reference.get('data');
  }

  Future<void> save(Map sessionData) async {
    final reference = await _localStorageManager.getReference(_sessionKey);
    reference.put('data', sessionData);
  }

  Future<void> logout() async {
    final reference = await _localStorageManager.getReference(_sessionKey);
    reference.deleteFromDisk();
  }
}
